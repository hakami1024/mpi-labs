#include "mpi.h"
#include <stdio.h>
#include <cassert>

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    assert(size >= 3);

    if(rank == 0) {

        int a[8][8];

        // generation:

//        srand(time(NULL));
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                a[i][j] = i; //rand()%10;
            }
        }

        // visualization:

        printf("A:\n");

        for(int i=0; i<8; i++){
            printf("[");
            for(int j=0; j<8; j++){
                printf("%d ", a[i][j]);
            }
            printf("]\n");
        }

        // transfer:

        MPI_Datatype OddEvenType;
        MPI_Type_vector(4, 8, 16, MPI_INT, &OddEvenType);
        MPI_Type_commit(&OddEvenType);

        MPI_Send(a, 1, OddEvenType, 1, 0, MPI_COMM_WORLD);
        MPI_Send(&(a[1]), 1, OddEvenType, 2, 0, MPI_COMM_WORLD);

        MPI_Type_free(&OddEvenType);
    } else if(rank == 1){
        int b[4][8];

        MPI_Recv(&b, 4*8, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        char printbuf[1000];

        int printn = sprintf(printbuf, "B:\n");

        for (int i = 0; i < 4; i++) {
            printn += sprintf(&(printbuf[printn]), "[");
            for (int j = 0; j < 8; j++) {
                printn += sprintf(&(printbuf[printn]), "%d ", b[i][j]);
            }
            printn += sprintf(&(printbuf[printn]), "]\n");
        }

        printf("%s", printbuf);

    } else if(rank == 2) {
        int c[4][8];

        MPI_Recv(&c, 4 * 8, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        char printbuf[1000];

        int printn = sprintf(printbuf, "C:\n");

        for (int i = 0; i < 4; i++) {
            printn += sprintf(&(printbuf[printn]), "[");
            for (int j = 0; j < 8; j++) {
                printn += sprintf(&(printbuf[printn]), "%d ", c[i][j]);
            }
            printn += sprintf(&(printbuf[printn]), "]\n");

        }

        printf("%s", printbuf);
    }

    MPI_Finalize();
    return 0;
}