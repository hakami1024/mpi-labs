#include "mpi.h"
#include <stdio.h>
#include <time.h>

int main(int argc,char** argv){
	int rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	int a[10];

	if(rank == 0){

		srand(time(nullptr));
		for(int i = 0; i<10; i++){
			a[i] = rand();
			printf("Generated %d: %d\n", i, a[i]);
		}

		MPI_Send(&a, 10, MPI_INT, 1, 0, MPI_COMM_WORLD);

	} else if (rank == 1){

		MPI_Recv(&a, 10, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(int i=0; i<10; i++){
			printf("Received %d: %d\n", i, a[i]);
		}
	}

	MPI_Finalize();
	return 0;
}