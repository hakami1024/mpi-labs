#include "mpi.h"
#include <stdio.h>


struct pair{
    double val;
    int pos;
};

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    int pcount = 20;

    double suba[pcount];

    if(rank == 0){
        int N = pcount*size;

        srand(time(NULL));
        double a[N];

        for(int i = 0; i<N; i++){
            a[i] = (rand()%200 - 100)/10.0;
            printf("Generated %d: %.1f\n", i, a[i]);
        }

        MPI_Scatter(&a, pcount, MPI_DOUBLE, &suba, pcount, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    } else {
        MPI_Scatter(nullptr, pcount, MPI_DOUBLE, &suba, pcount, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    struct pair min;
    min.val = suba[0];
    min.pos = pcount*rank;

    for(int i=1; i<pcount; i++){
        if(min.val > suba[i]){
            min.val = suba[i];
            min.pos = pcount*rank + i;
        }
    }

    if(rank == 0){
        struct pair result;
        MPI_Reduce(&min, &result, 1, MPI_DOUBLE_INT, MPI_MINLOC, 0, MPI_COMM_WORLD);
        printf("\nMinimum located at %d is %.1f\n", result.pos, result.val);
    } else {
        MPI_Reduce(&min, nullptr, 1, MPI_DOUBLE_INT, MPI_MINLOC, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}