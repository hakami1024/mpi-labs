#include "mpi.h"
#include <stdio.h>

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    const int N=8;

    MPI_Datatype TriangleType;

    if(rank == 0) {
        int a[N][N] = {};

        int disp[N], blocklens[N];

        // generation:

        srand(time(NULL));
        for (int i = 0; i < N; i++) {
            for (int j = i; j < N; j++) {
                a[i][j] = 1 + rand() % 9;
            }

            disp[i] = i*N+i;
            blocklens[i] = N-i;
        }

        // visualization:

        printf("A:\n");

        for (int i = 0; i < N; i++) {
            printf("[");
            for (int j = 0; j < N; j++) {
                printf("%d ", a[i][j]);
            }
            printf("]\n");
        }

        MPI_Type_indexed(N, blocklens, disp, MPI_INT, &TriangleType);
        MPI_Type_commit(&TriangleType);

        MPI_Bcast(&a, 1, TriangleType, 0, MPI_COMM_WORLD);

        MPI_Type_free(&TriangleType);
    } else {
        int recvN = N*(N+1)/2;
        int recv[recvN];
        MPI_Bcast(&recv, recvN, MPI_INT, 0, MPI_COMM_WORLD);

        int recvA[N][N] = {};

        int q=0;
        for(int i=0; i<N; i++){
            for(int j=i; j<N; j++, q++){
                recvA[i][j] = recv[q];
            }
        }

        char printbuf[1000];

        int printn = sprintf(printbuf, "Proc%d received A:\n", rank);

        for(int i=0; i<N; i++){
            printn += sprintf(&(printbuf[printn]), "[");
            for (int j = 0; j < N; j++) {
                printn += sprintf(&(printbuf[printn]), "%d ", recvA[i][j]);
            }
            printn += sprintf(&(printbuf[printn]), "]\n");
        }

        printf("%s", printbuf);
    }


    MPI_Finalize();
    return 0;
}