#include "mpi.h"
#include <stdio.h>

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    // A[m x n] * x[n x 1]

    int prows = 3;
    int m = size*prows;
    int n = 16;

    int suba[prows][n];
    int x[n];

    if(rank == 0) {

        int a[m][n];

        // generation:

        srand(time(NULL));
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < m; i++) {
                a[i][j] = rand()%10;
            }

            x[j] = rand()%10;
        }

        // visualization:

        printf("A:\n");

        for(int i=0; i<m; i++){
            printf("[");
            for(int j=0; j<n; j++){
                printf("%d ", a[i][j]);
            }
            printf("]\n");
        }

        printf("x: \n");
        for(int j=0; j<n; j++){
            printf("%d ", x[j]);
        }
        printf("\n");


        MPI_Scatter(&a, prows*n, MPI_INT, &suba, prows*n, MPI_INT, 0, MPI_COMM_WORLD);
    } else {
        MPI_Scatter(nullptr, prows*n, MPI_INT, &suba, prows*n, MPI_INT, 0, MPI_COMM_WORLD);
    }

    MPI_Bcast(&x, n, MPI_INT, 0, MPI_COMM_WORLD);

    int subres[prows];

    int pstart = rank*prows;

    for(int i=0; i<prows; i++){
        subres[i] = 0;

        for(int j=0; j<n; j++){
            subres[i] += suba[i][j]*x[j];
        }
    }

    if(rank == 0){
        int result[m];
        MPI_Gather(&subres, prows, MPI_INT, &result, prows, MPI_INT, 0, MPI_COMM_WORLD);

        printf("A * x =\n");

        for(int i=0; i<m; i++){
            printf("%d ", result[i]);
        }

        printf("\n");
    } else {
        MPI_Gather(&subres, prows, MPI_INT, nullptr, prows, MPI_INT, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}