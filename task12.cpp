#include "mpi.h"
#include <stdio.h>
#include <cassert>
#include <stddef.h>     /* offsetof */

struct Pair{
    int first_line[8];
    int second_line[8];
};

using namespace std;

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    assert(size >= 5);

    struct Pair pair;

    MPI_Datatype PairType;

    int blocklens[2] = {8, 8};
    MPI_Aint offsets[2] = {offsetof(Pair, first_line), offsetof(Pair, second_line)};
    MPI_Datatype types[2] = {MPI_INT, MPI_INT};
    MPI_Type_create_struct(2, blocklens, offsets, types, &PairType);
    MPI_Type_commit(&PairType);


    if(rank == 4) {

        int a[8][8];

        // generation:

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                a[i][j] = i;
            }
        }

        // visualization:

        printf("A:\n");

        for(int i=0; i<8; i++){
            printf("[");
            for(int j=0; j<8; j++){
                printf("%d ", a[i][j]);
            }
            printf("]\n");
        }

        // transfer:

        for(int i=0; i<4; i++){
            copy(a[i], a[i]+8, pair.first_line);
            copy(a[i+4], a[i+4]+8, pair.second_line);
            MPI_Send(&pair, 1, PairType, i, 0, MPI_COMM_WORLD);
        }

    } else if(rank < 4){

        MPI_Recv(&pair, 1, PairType, 4, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        char printbuf[1000];

        int printn = sprintf(printbuf, "D%d:\n", rank);

        int d[2][8];
        copy(pair.first_line, pair.first_line+8, d[0]);
        copy(pair.second_line, pair.second_line+8, d[1]);

        for(int i=0; i<2; i++){
            printn += sprintf(&(printbuf[printn]), "[");
            for (int j = 0; j < 8; j++) {
                printn += sprintf(&(printbuf[printn]), "%d ", d[i][j]);
            }
            printn += sprintf(&(printbuf[printn]), "]\n");
        }

        printf("%s", printbuf);

    }

    MPI_Type_free(&PairType);

    MPI_Finalize();
    return 0;
}