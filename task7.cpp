#include "mpi.h"
#include <stdio.h>
#include <cmath>


int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    int pcount = 10;
    int N = size * pcount;

    int subx[pcount];

    if(rank == 0) {
        int x[N];

        for (int i = 0; i < N; i++) {
            x[i] = i-2*(i%2)*i;
            printf("%d, ", x[i]);
        }

        MPI_Scatter(&x, pcount, MPI_INT, &subx, pcount, MPI_INT, 0, MPI_COMM_WORLD);

        printf("\n");
    } else {
        MPI_Scatter(nullptr, pcount, MPI_INT, &subx, pcount, MPI_INT, 0, MPI_COMM_WORLD);
    }



    int sum = 0;
    for(int i=0; i<pcount; i++){
        sum += abs(subx[i]);
    }

    if(rank == 0){
        int result;
        MPI_Reduce(&sum, &result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        printf("\nl-1 norm is %d\n", result);
    } else {
        MPI_Reduce(&sum, nullptr, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}