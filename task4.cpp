#include "mpi.h"
#include <stdio.h>
#include <time.h>

const int TASK_SUM = 1;
const int TASK_MULT = 2;

const int TASK = TASK_SUM;

int main(int argc,char** argv){
	int size,rank;
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);


	if(rank == 0){

		const int ar_len = 10;
		int x[ar_len], y[ar_len], res[ar_len];

		srand(time(NULL));
		for(int i = 0; i<ar_len; i++){
			x[i] = rand() % 100;
			y[i] = rand() % 100;
			printf("Generated %d: (%d, %d)\n", i, x[i], y[i]);
		}

		int part_len = ar_len / (size-1);
		int last_part_loc = part_len*(size-2);
		int last_part_len = ar_len - last_part_loc;

		for(int i = 1; i<size-1; i++){
			int start_loc = part_len*(i-1);
			MPI_Send(&x[start_loc], part_len, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send(&y[start_loc], part_len, MPI_INT, i, 0, MPI_COMM_WORLD);		
		}
	
		MPI_Send(&x[last_part_loc], last_part_len, MPI_INT, size-1, 0, MPI_COMM_WORLD);
		MPI_Send(&y[last_part_loc], last_part_len, MPI_INT, size-1, 0, MPI_COMM_WORLD);		
		
		for(int i=1; i<size-1; i++){
			int start_loc = (i-1)*part_len;
			MPI_Recv(&res[start_loc], part_len, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}

		MPI_Recv(&res[last_part_loc], last_part_len, MPI_INT, size-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		char op_name = TASK == TASK_SUM ? '+' : '*';

		for(int i=0; i<ar_len; i++){
			printf("Got result: %d %c %d = %d\n", x[i], op_name, y[i], res[i]);
		}

	} else {

		MPI_Status status;
		MPI_Probe(0, 0, MPI_COMM_WORLD, &status);

		int n;
		MPI_Get_count(&status, MPI_INT, &n);
		int x[n], y[n], res[n];

		MPI_Recv(&x, n, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&y, n, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(int i=0; i<n; i++){
			printf("Process %d Received %d: (%d, %d)\n", rank, i, x[i], y[i]);
		}

		for(int i=0; i<n; i++){
			switch(TASK){
				case TASK_SUM:
					res[i] = x[i] + y[i];
					continue;
				case TASK_MULT:
					res[i] = x[i] * y[i];
					continue;
				default:break;
			}
		}

		MPI_Send(&res, n, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();
	return 0;
}