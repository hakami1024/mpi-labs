#include "mpi.h"
#include <stdio.h>
#include <time.h>

int main(int argc,char** argv){
	int size,rank;
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);


	if(rank == 1){

		int a[10];

		srand(time(NULL));
		for(int i = 0; i<10; i++){
			a[i] = rand() % 100;
			printf("Generated %d: %d\n", i, a[i]);
		}

		for(int i = 0; i<size; i++){
			if(i != 1) {
				MPI_Send(&a, 10, MPI_INT, i, 0, MPI_COMM_WORLD);
			}
		}

	} else {

		MPI_Status status;
		MPI_Probe(1, 0, MPI_COMM_WORLD, &status);

		int n;
		MPI_Get_count(&status, MPI_INT, &n);
		int a[n];

		MPI_Recv(&a, 10, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(int i=0; i<n; i++){
			printf("Process %d Received %d: %d\n", rank, i, a[i]);
		}
	}

	MPI_Finalize();
	return 0;
}