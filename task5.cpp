#include "mpi.h"
#include <stdio.h>
int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    size--;
    const int pcount = 2;
    int N = size*pcount;
    int partlen = pcount*N;

    int diag[pcount];

    if(rank == 0){
        int a[N][N];
        int x[N];

        // generation:
        srand(time(NULL));
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                a[i][j] = rand() % 10;
            }
        }

        // visualization:
        printf("A:\n");

        for (int i = 0; i < N; i++) {
            printf("[");
            for (int j = 0; j < N; j++) {
                printf("%d ", a[i][j]);
            }
            printf("]\n");
        }

        // transfer:
        for(int i=0; i<size; i++){
            MPI_Send(&(a[i*pcount]), partlen, MPI_INT, i+1, 0, MPI_COMM_WORLD);
        }

        for(int i=0; i<size; i++){
            MPI_Recv(&(x[i*pcount]), pcount, MPI_INT, i+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        printf("X:\n");
        for(int i=0; i<N; i++){
            printf("%d ", x[i]);
        }
        printf("\n");
    } else {
        int suba[pcount][N];

        MPI_Recv(&suba, partlen, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (int i = 0; i < pcount; i++) {
            diag[i] = suba[i][i+(rank-1)*pcount];
//            printf("computed (%d) %d on rank %d \n", i, diag[i], rank);
        }

        MPI_Send(&diag, pcount, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }


    MPI_Finalize();
    return 0;
}