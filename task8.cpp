#include "mpi.h"
#include <stdio.h>
int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    const int pcount = 2;
    
    int subx[pcount], suby[pcount];    

    if(rank == 0){
        int N = pcount*size;
        int x[N], y[N];
        
        srand(time(NULL));
        for(int i = 0; i<N; i++){
            x[i] = rand()%20 - 10;
            y[i] = rand()%20 - 10;
            printf("Generated %d: (%d, %d)\n", i, x[i], y[i]);
        }

        MPI_Scatter(&x, pcount, MPI_INT, &subx, pcount, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(&y, pcount, MPI_INT, &suby, pcount, MPI_INT, 0, MPI_COMM_WORLD);
    } else {
        MPI_Scatter(nullptr, pcount, MPI_INT, &subx, pcount, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(nullptr, pcount, MPI_INT, &suby, pcount, MPI_INT, 0, MPI_COMM_WORLD);
    }
    
    int presult = 0;
    
    for(int i=0; i<pcount; i++){
        presult += subx[i]*suby[i];
    }

    if(rank == 0){
        int result;
        MPI_Reduce(&presult, &result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        printf("\nDot-product is %d\n", result);
    } else {
        MPI_Reduce(&presult, nullptr, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}