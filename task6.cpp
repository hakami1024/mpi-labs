#include "mpi.h"
#include <stdio.h>
#include <time.h>

const int TASK_PING_PING = 1;
const int TASK_PING_PONG = 2;

int TASK = TASK_PING_PING;

const int COUNT = 1;

using namespace std;

void ping_pong(int rank){

    int N_MAX = 1048576; // = 2^20

    if(rank == 0){

        printf("n \t| avg seconds\n-------------------------------------------\n");

        for(int n = 2; n < N_MAX; n*=2) {

            char message[n];

            srand(time(NULL));
            for (int i = 0; i < n; i++) {
                message[i] = rand() % 128;
            }

            clock_t begin = clock();

            for (int q = 0; q < COUNT; q++) {
                MPI_Send(&message, n, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
                MPI_Recv(&message, n, MPI_CHAR, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }

            clock_t end = clock();
            double secs = double(end - begin) / CLOCKS_PER_SEC;

            printf("%d\t|%f\n", n, secs/COUNT);
        }
    } else if (rank == 1){

        for(int n = 2; n < N_MAX; n*=2) {

            char message[n];

            for (int q = 0; q < COUNT; q++) {
                MPI_Recv(&message, n, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Send(&message, n, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
            }
        }
    }
}

void ping_ping(int rank){

//    int N_MAX = 4096; // no buffer is available for ns greater than 2048. Without buffer deadlock occurs.
    int N_MAX = 1048576; // = 2^20

    if(rank == 0){

        printf("n \t| avg seconds\n------------------------------\n");

        for(int n = 2; n < N_MAX; n*=2) {

            char message[n], input[n];

            srand(time(NULL));
            for (int i = 0; i < n; i++) {
                message[i] = rand() % 128;
            }

            MPI_Barrier(MPI_COMM_WORLD);
            clock_t begin = clock();

            for (int q = 0; q < COUNT; q++) {
                MPI_Request request;
                MPI_Isend(&message, n, MPI_CHAR, 1, q, MPI_COMM_WORLD, &request);
                MPI_Irecv(&input, n, MPI_CHAR, 1, q, MPI_COMM_WORLD, &request);

                MPI_Wait(&request, MPI_STATUS_IGNORE);
            }

            MPI_Barrier(MPI_COMM_WORLD);
            clock_t end = clock();

//            clock_t other_begin, other_end;
//
//            MPI_Recv(&other_begin, 1, MPI_UNSIGNED_LONG_LONG, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//            MPI_Recv(&other_end, 1, MPI_UNSIGNED_LONG_LONG, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

//            double secs = double(max(end, other_end) - min(begin, other_begin)) / CLOCKS_PER_SEC;
            double secs = double(end - begin) / CLOCKS_PER_SEC;

            printf("%d\t|%f\n", n, secs/COUNT);
        }
    } else if (rank == 1){

        for(int n = 2; n < N_MAX; n*=2) {
            char message[n], input[n];

            srand(time(NULL));
            for (int i = 0; i < n; i++) {
                message[i] = rand() % 128;
            }

//            clock_t begin = clock();

            MPI_Barrier(MPI_COMM_WORLD);

            for (int q = 0; q < COUNT; q++) {
                MPI_Request request;

                MPI_Isend(&message, n, MPI_CHAR, 0, q, MPI_COMM_WORLD, &request);
                MPI_Irecv(&input, n, MPI_CHAR, 0, q, MPI_COMM_WORLD, &request);

                MPI_Wait(&request, MPI_STATUS_IGNORE);
            }

            MPI_Barrier(MPI_COMM_WORLD);

//            clock_t end = clock();
//
//            MPI_Send(&begin, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
//            MPI_Send(&end, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
        }
    }
}

int main(int argc,char** argv){
    int size,rank;
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if(size < 2){
        printf("Run the program with at least 2 processes");
        MPI_Finalize();
        return 0;
    }
    
    if(TASK == TASK_PING_PONG){
        ping_pong(rank);
    } else {
        ping_ping(rank);
    }

    MPI_Finalize();
    return 0;
}