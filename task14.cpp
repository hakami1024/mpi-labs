#include "mpi.h"
#include <stdio.h>
#include <ctime>

int main(int argc, char **argv) {
    int size, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int ints[10];
    double doubles[8];

    int bN = 10 * sizeof(int) + 8 * sizeof(double);
    char buff[bN];

    if (rank == 0) {

        // generation & visualisation

        srand(time(0));

        printf("Ints:\n");
        for (int i = 0; i < 10; i++) {
            ints[i] = rand() % 100;
            printf("%d ", ints[i]);
        }

        printf("\nDoubles:\n");
        for (int i = 0; i < 8; i++) {
            doubles[i] = rand() % 100 / 10.0;
            printf("%.1f ", doubles[i]);
        }
        printf("\n");

        // transfer
        int pos = 0;
        MPI_Pack(ints, 10, MPI_INT, &buff, bN, &pos, MPI_COMM_WORLD);
        MPI_Pack(doubles, 8, MPI_DOUBLE, &buff, bN, &pos, MPI_COMM_WORLD);
    }

    MPI_Bcast(&buff, bN, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (rank != 0) {
        int pos = 0;
        MPI_Unpack(&buff, bN, &pos, &ints, 10, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(&buff, bN, &pos, &doubles, 8, MPI_DOUBLE, MPI_COMM_WORLD);

        char printbuf[1000];

        int printn = sprintf(printbuf, "Proc%d received ints: ", rank);

        for (int i = 0; i < 10; i++) {
            printn += sprintf(&(printbuf[printn]), "%d ", ints[i]);
        }

        printn += sprintf(&(printbuf[printn]), "\n\tAnd doubles: ");

        for (int i = 0; i < 8; i++) {
            printn += sprintf(&(printbuf[printn]), "%.1f ", doubles[i]);
        }

        printf("%s\n", printbuf);
    }

    MPI_Finalize();
    return 0;
}